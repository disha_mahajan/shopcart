import React, {Component, Fragment} from 'react';
import {
  View,
  Dimensions,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import NavigationBar from 'react-native-navbar-color';
import EStyleSheet from 'react-native-extended-stylesheet';
import {DrawerActions } from 'react-navigation-drawer';
import {SliderBox} from 'react-native-image-slider-box';
import {fontSize} from '../components/functions/Fontsize';
import Colors from '../styles/colors';
import Styles from '../styles/styles';
import STRINGS from '../utils/strings';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class home extends Component {
  constructor() {
    super();
    this.state = {
      statusBarColor: Colors.WHITE,
      sliderImages: [
        require('../assets/1.jpg'),
        require('../assets/2.png'),
        require('../assets/3.jpg'),
        require('../assets/4.jpg'),
      ],
    };
  }

  toggleDrawer = () => {
    this.setState({statusBarColor: Colors.WHITE});
    this.props.navigation.dispatch(DrawerActions.toggleDrawer());

  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('#333333');
    this.onload();
  }

  onload = () => {
    this.props.navigation.addListener('didFocus', () => {
      this.setState({statusBarColor: Colors.WHITE});
      NavigationBar.setStatusBarColor('white',false)
      NavigationBar.setColor('#333333');
    });
  };

  render() {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar
            backgroundColor={this.state.statusBarColor}
            barStyle="dark-content"
          />
          <View style={styles.headerContent}>
            <TouchableOpacity onPress={() => this.toggleDrawer()}>
              <Image
                source={require('../assets/menu.png')}
                style={styles.hamburgerIcon}
                // tintColor={Colors.BLACK}
              />
            </TouchableOpacity>
            <View style={styles.header}>
              <Text style={styles.shop}>
                {STRINGS.login.shop}
                <Text style={styles.cart}>{STRINGS.login.cart}</Text>
              </Text>
            </View>
          </View>
          <ScrollView>
            <SliderBox
              images={this.state.sliderImages}
              sliderBoxHeight={300}
              resizeMode="cover"
              circleLoop
              autoplay
              useScrollView
            />
            <View style={styles.rowView}>
              <TouchableOpacity style={styles.columnView}>
                <Image
                  source={require('../assets/poster2.png')}
                  style={styles.poster1}
                />
              </TouchableOpacity>
              <View style={styles.columnView}>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/poster1.png')}
                    style={styles.poster2}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/poster3.png')}
                    style={styles.poster2}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.topDealsView}>
              <Text style={styles.topDeals}>{'Top Deals of an hour ..!'}</Text>
            </View>
            <View style={styles.rowView}>
              <TouchableOpacity style={styles.columnView}>
                <Image
                  source={require('../assets/poster2.png')}
                  style={styles.poster1}
                />
              </TouchableOpacity>
              <View style={styles.columnView}>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/poster1.png')}
                    style={styles.poster2}
                  />
                </TouchableOpacity>
                <TouchableOpacity>
                  <Image
                    source={require('../assets/poster3.png')}
                    style={styles.poster2}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default home;

const styles = EStyleSheet.create({
  headerContent: {
    alignContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
    height: '60rem',
    paddingLeft: '15rem',
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  shop: {
    marginLeft: '10rem',
    color: Colors.SKY_BLUE,
    fontFamily: 'Roboto-Medium',
    fontSize: '23rem',
  },
  cart: {
    color: Colors.GREEN,
    fontFamily: 'Roboto-Medium',
    fontSize: '23rem',
  },
  hamburgerIcon: {
    width: '20rem',
    height: '20rem',
  },
  rowView: {
    flexDirection: 'row',
    flex: 1,
  },
  poster1: {
    height: '300rem',
    width: '200rem',
  },
  poster2: {
    height: '150rem',
    width: '200rem',
  },
  columnView: {
    flex: 1,
    flexDirection: 'column',
    borderWidth: 2,
    borderColor: Colors.WHITE,
  },
  topDealsView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '10rem',
    elevation: 20,
    backgroundColor: '#ccc',
  },
  topDeals: {
    color: Colors.SKY_BLUE,
    fontFamily: 'Roboto-Regular',
    fontSize: '20rem',
  },
});
