import React, {Component, Fragment} from 'react';
import {
  View,
  Dimensions,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Text,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {TouchableOpacity} from 'react-native-gesture-handler';
import NavigationBar from 'react-native-navbar-color';
import {SliderBox} from 'react-native-image-slider-box';
import {fontSize} from '../components/functions/Fontsize';
import Colors from '../styles/colors';
import Styles from '../styles/styles';
import STRINGS from '../utils/strings';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class welcomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      sliderImages: [
        // 'https://source.unsplash.com/1024x768/?nature',
        // 'https://source.unsplash.com/1024x768/?water',
        // 'https://source.unsplash.com/1024x768/?girl',
        // 'https://source.unsplash.com/1024x768/?tree',
        require('../assets/1.jpg'),
        require('../assets/2.png'),
        require('../assets/3.jpg'),
        require('../assets/4.jpg'),
      ],
    };
  }
  
  navigateToLogin = () => {
    this.props.navigation.navigate('Auth');
  };

  render() {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar
            backgroundColor={Colors.WHITE}
            barStyle="dark-content"
          />
          <ScrollView>
            <SliderBox
              images={this.state.sliderImages}
              sliderBoxHeight={500}
              resizeMode="cover"
              circleLoop
              autoplay
              useScrollView
            />
            <View style={styles.welcomeView}>
              <Text style={styles.welcomeTo}>
                {STRINGS.welcome.welcomeTo}
                <Text style={styles.shopcart}>{STRINGS.welcome.shopcart}</Text>
              </Text>
            </View>
            <View style={styles.dashView}>
              <View style={styles.dashStyle}></View>
            </View>
            <View style={styles.welcomeTextView}>
              <Text style={styles.welcomeText}>
                {STRINGS.welcome.welcomeText}
              </Text>
            </View>
          </ScrollView>
          <TouchableOpacity
            style={styles.getStartedBtn}
            onPress={this.navigateToLogin}>
            <Text style={styles.getStarted}>{STRINGS.welcome.getStarted}</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default welcomeScreen;

const styles = EStyleSheet.create({
  contentContainer: {
    flex: 1,
  },
  getStartedBtn: {
    backgroundColor: Colors.LIGHT_SKYBLUE,
    height: '60rem',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  getStarted: {
    color: Colors.WHITE,
    fontFamily: 'Roboto-Bold',
    letterSpacing: '0.8rem',
    fontSize: '15rem',
  },
  welcomeTo: {
    color: Colors.BLACK,
    fontFamily: 'Roboto-Regular',
    fontSize: '25rem',
  },
  welcomeText: {
    color: Colors.GREY_TEXT,
    fontFamily: 'Roboto-Regular',
    fontSize: '16rem',
    textAlign: 'center',
    lineHeight: '30rem',
  },
  shopcart: {
    color: Colors.BLACK,
    fontFamily: 'Roboto-Medium',
    fontSize: '25rem',
  },
  welcomeView: {
    marginTop: '30rem',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '10rem',
  },
  welcomeTextView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '10rem',
    marginRight: '20rem',
    marginLeft: '20rem',
  },
  dashStyle: {
    borderColor: Colors.GREEN,
    borderWidth: 1,
    width: '35rem',
  },
  dashView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '10rem',
  },
});
