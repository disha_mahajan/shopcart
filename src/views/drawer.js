import React, {Component, Fragment} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Image,
  Dimensions,
  StatusBar,
} from 'react-native';
import {StackActions, NavigationActions} from 'react-navigation';
import NavigatioBar from 'react-native-navbar-color';
import Styles from '../styles/styles';
import Colors from '../styles/colors';
import {fontSize} from '../components/functions/Fontsize';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../utils/strings';
import {TouchableOpacity} from 'react-native-gesture-handler';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();
class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   statusBarColor: Colors.LIGHT_SKYBLUE,
      openMensWear: false,
      openWomensWear: false,
      openAccesories: false,
      shoes: false,
      clothing: false,
      walletsAndBags: false,
      westernWear: false,
      jewellery: false,
      sunglassesAndFrames: false,
      capHats: false,
      cosmetics: false,
      trackOrder: false,
      accDetails: false,
      settings: false,
      signOut: false,
    };
  }

  componentDidMount(){
    NavigatioBar.setColor(Colors.LIGHT_SKYBLUE)
  }

  onClickMensWear = () => {
    this.setState({
      openMensWear: !this.state.openMensWear,
    });
  };

  onClickWomensWear = () => {
    this.setState({
      openWomensWear: !this.state.openWomensWear,
    });
  };

  onClickAccescories = () => {
    this.setState({
      openAccesories: !this.state.openAccesories,
    });
  };

  onClickOption = (option) => {
    switch (option) {
      case 'shoes':
        this.setState({
          shoes: true,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'walletsAndBags':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: true,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'jewellery':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: true,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'capHats':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: true,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'sunglassesAndFrames':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: true,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'clothing':
        this.setState({
          shoes: false,
          clothing: true,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'westernWear':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: true,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'cosmetics':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: true,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'trackOrder':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: true,
          accDetails: false,
          settings: false,
          signOut: false,
        });
        break;
      case 'accDetails':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: true,
          settings: false,
          signOut: false,
        });
        break;
      case 'settings':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: true,
          signOut: false,
        });
        break;
      case 'signOut':
        this.setState({
          shoes: false,
          clothing: false,
          walletsAndBags: false,
          westernWear: false,
          jewellery: false,
          sunglassesAndFrames: false,
          capHats: false,
          cosmetics: false,
          trackOrder: false,
          accDetails: false,
          settings: false,
          signOut: true,
        });
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <Fragment>
        <View style={Styles.screen}>
          <StatusBar
            backgroundColor={Colors.LIGHT_SKYBLUE}
            barStyle="light-content"
          />
          <SafeAreaView style={{backgroundColor: Colors.LIGHT_SKYBLUE}} />
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.sideMenuContainer}>
            <View style={styles.container}>
              <TouchableOpacity
                style={styles.labelContainer}
                onPress={this.onClickMensWear}>
                <Text style={styles.labelText}>{STRINGS.home.mensWear}</Text>
                <Image
                  source={
                    this.state.openMensWear == false
                      ? require('../assets/plus.png')
                      : require('../assets/minus.png')
                  }
                  style={styles.labelImage}
                  tintColor={Colors.WHITE}
                />
              </TouchableOpacity>
              {this.state.openMensWear && (
                <TouchableOpacity
                  style={
                    this.state.shoes == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('shoes');
                  }}>
                  <Text style={styles.subLabelText}>{STRINGS.home.shoes}</Text>
                </TouchableOpacity>
              )}
              {this.state.openMensWear && (
                <TouchableOpacity
                  style={
                    this.state.clothing == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('clothing');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.clothing}
                  </Text>
                </TouchableOpacity>
              )}

              <TouchableOpacity
                style={styles.labelContainer}
                onPress={this.onClickWomensWear}>
                <Text style={styles.labelText}>{STRINGS.home.womensWear}</Text>
                <Image
                  source={
                    this.state.openWomensWear == false
                      ? require('../assets/plus.png')
                      : require('../assets/minus.png')
                  }
                  style={styles.labelImage}
                  tintColor={Colors.WHITE}
                />
              </TouchableOpacity>

              {this.state.openWomensWear && (
                <TouchableOpacity
                  style={
                    this.state.walletsAndBags == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('walletsAndBags');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.walletsAndBags}
                  </Text>
                </TouchableOpacity>
              )}
              {this.state.openWomensWear && (
                <TouchableOpacity
                  style={
                    this.state.westernWear == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('westernWear');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.westernWear}
                  </Text>
                </TouchableOpacity>
              )}
              {this.state.openWomensWear && (
                <TouchableOpacity
                  style={
                    this.state.jewellery == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('jewellery');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.jewellery}
                  </Text>
                </TouchableOpacity>
              )}

              <TouchableOpacity
                style={styles.labelContainer}
                onPress={this.onClickAccescories}>
                <Text style={styles.labelText}>{STRINGS.home.accesories}</Text>
                <Image
                  source={
                    this.state.openAccesories == false
                      ? require('../assets/plus.png')
                      : require('../assets/minus.png')
                  }
                  style={styles.labelImage}
                  tintColor={Colors.WHITE}
                />
              </TouchableOpacity>

              {this.state.openAccesories && (
                <TouchableOpacity
                  style={
                    this.state.sunglassesAndFrames == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('sunglassesAndFrames');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.sunglassesAndFrames}
                  </Text>
                </TouchableOpacity>
              )}
              {this.state.openAccesories && (
                <TouchableOpacity
                  style={
                    this.state.capHats == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('capHats');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.capHats}
                  </Text>
                </TouchableOpacity>
              )}
              {this.state.openAccesories && (
                <TouchableOpacity
                  style={
                    this.state.cosmetics == true
                      ? styles.activeLabelContainer
                      : styles.labelContainer
                  }
                  onPress={() => {
                    this.onClickOption('cosmetics');
                  }}>
                  <Text style={styles.subLabelText}>
                    {STRINGS.home.cosmetics}
                  </Text>
                </TouchableOpacity>
              )}

              <TouchableOpacity
                style={
                  this.state.trackOrder == true
                    ? styles.activeLabelContainer
                    : styles.labelContainer
                }
                onPress={() => {
                  this.onClickOption('trackOrder');
                }}>
                <Text style={styles.labelText}>{STRINGS.home.trackOrder}</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.accDetails == true
                    ? styles.activeLabelContainer
                    : styles.labelContainer
                }
                onPress={() => {
                  this.onClickOption('accDetails');
                }}>
                <Text style={styles.labelText}>
                  {STRINGS.home.accountDetails}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.settings == true
                    ? styles.activeLabelContainer
                    : styles.labelContainer
                }
                onPress={() => {
                  this.onClickOption('settings');
                }}>
                <Text style={styles.labelText}>{STRINGS.home.settings}</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={
                  this.state.signOut == true
                    ? styles.activeLabelContainer
                    : styles.labelContainer
                }
                onPress={() => {
                  this.onClickOption('signOut');
                }}
                onPress={() => {
                  this.props.navigation.navigate('Auth');
                  const resetAction = StackActions.reset({
                    index: 0,
                    key: 'Auth',
                    actions: [NavigationActions.navigate({routeName: 'Auth'})],
                  });
                  this.props.navigation.dispatch(resetAction);
                }}>
                <Text style={styles.labelText}>{STRINGS.home.signOut}</Text>
              </TouchableOpacity>
            </View>
            <View style={{height: 50}} />
          </ScrollView>
        </View>
      </Fragment>
    );
  }
}

export default Drawer;

const styles = EStyleSheet.create({
  labelImage: {
    height: '10rem',
    width: '10rem',
    marginRight: '20rem',
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: '60rem',
    alignItems: 'center',
    borderBottomWidth: 0.2,
    borderBottomColor: 'rgba(23,177,237,0.5)',
  },
  activeLabelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: '60rem',
    alignItems: 'center',
    backgroundColor: Colors.ACTIVE_TAB_COLOR,
    borderLeftWidth: 3,
    borderLeftColor: Colors.WHITE,
  },
  container: {
    marginTop: '10rem',
    flex: 1,
    backgroundColor: Colors.LIGHT_SKYBLUE,
  },
  labelText: {
    color: Colors.WHITE,
    fontSize: fontSizeValue * 11.5,
    fontFamily: 'Roboto-Thin',
    paddingLeft: '15rem',
  },
  subLabelText: {
    color: Colors.WHITE,
    fontSize: fontSizeValue * 10,
    fontFamily: 'Roboto-Thin',
    paddingLeft: '15rem',
  },
  sideMenuContainer: {
    flex: 1,
    backgroundColor: Colors.LIGHT_SKYBLUE,
  },
});
