import React, {Component, Fragment} from 'react';
import {
  View,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  Image,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import Colors from '../styles/colors';
import Styles from '../styles/styles';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});

class splashScreen extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    NavigationBar.setColor(Colors.LIGHT_SKYBLUE);
    setTimeout(() => {
        this.props.navigation.navigate('Welcome');
      }, 2000);
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={Styles.splashScreen}>
          <StatusBar backgroundColor={Colors.LIGHT_SKYBLUE} barStyle="light-content" />
          <ImageBackground
            source={require('../assets/launch_screen.png')}
            style={styles.imgBackground}>
          </ImageBackground>
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default splashScreen;

const styles = EStyleSheet.create({
  imgBackground: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
});
