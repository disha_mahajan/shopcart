import React, {Component, Fragment} from 'react';
import {
  View,
  Dimensions,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Text,
  // TouchableOpacity,
  Image,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {TextField} from 'react-native-material-textfield';
import NavigationBar from 'react-native-navbar-color';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {fontSize} from '../components/functions/Fontsize';
import Colors from '../styles/colors';
import Styles from '../styles/styles';
import STRINGS from '../utils/strings';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class welcomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      selectedScreen: true,
      password: '',
      email: '',
      secureTextEntry: true,
      registerName: '',
      registerEmail: '',
      registerNumber: '',
      registerPassword: '',
    };
  }

  componentDidMount() {
    NavigationBar.setStatusBarColor('white', false);
    NavigationBar.setColor('white');
    this.setState({selectedScreen: true});
  }

  navigateToHome = () => {
    this.props.navigation.navigate('Home');
  };

  switchToSignin = () => {
    this.setState({selectedScreen: true});
  };

  switchToSignup = () => {
    this.setState({selectedScreen: false});
  };

  onSwipeLeft(gestureState) {
    this.setState({selectedScreen: false});
  }

  onSwipeRight(gestureState) {
    this.setState({selectedScreen: true});
  }

  onSwipe(gestureName, gestureState) {
    const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    switch (gestureName) {
      case SWIPE_LEFT:
        this.setState({selectedScreen: false});
        break;
      case SWIPE_RIGHT:
        this.setState({selectedScreen: true});
        break;
    }
  }

  visiblePassword = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar backgroundColor={Colors.WHITE} barStyle="dark-content" />
          <View style={styles.header}>
            <View style={styles.logoTitle}>
              <Image
                source={require('../assets/logo.png')}
                style={styles.logoStyle}
              />
              <Text style={styles.shop}>
                {STRINGS.login.shop}
                <Text style={styles.cart}>{STRINGS.login.cart}</Text>
              </Text>
            </View>
            <TouchableOpacity onPress={this.navigateToHome}>
              <Text style={styles.skip}>{STRINGS.login.skip}</Text>
            </TouchableOpacity>
          </View>
          <ScrollView>
            <GestureRecognizer
              onSwipe={(direction, state) => this.onSwipe(direction, state)}
              onSwipeLeft={(state) => this.onSwipeLeft(state)}
              onSwipeRight={(state) => this.onSwipeRight(state)}
              config={config}>
              <View>
                <View style={styles.headerTitleRow}>
                  <Text style={styles.headerTitle}>
                    {this.state.selectedScreen == true
                      ? STRINGS.login.signIn
                      : STRINGS.login.signUp}
                  </Text>
                </View>
                <View style={styles.dashView}>
                  <TouchableOpacity
                    hitSlop={{top: 100, bottom: 100, left: 50, right: 50}}
                    onPress={this.switchToSignin}
                    style={
                      this.state.selectedScreen == true
                        ? styles.dashStyle
                        : styles.dashStyleGrey
                    }></TouchableOpacity>
                  <TouchableOpacity
                    hitSlop={{top: 100, bottom: 100, left: 50, right: 50}}
                    onPress={this.switchToSignup}
                    style={
                      this.state.selectedScreen == false
                        ? styles.dashStyle1
                        : styles.dashStyleGrey1
                    }></TouchableOpacity>
                </View>
                {this.state.selectedScreen && (
                  <View style={styles.signIn}>
                    <View style={[styles.inputStyles, {marginBottom: '10%'}]}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/email.png')}
                          tintColor={
                            this.state.email.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.email}
                          lineWidth={1}
                          onChangeText={(email) => {
                            this.setState({email: email});
                          }}
                          placeholderTextColor={Colors.SKY_BLUE}
                          style={styles.placeholderStyle}
                          value={this.state.email}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                    </View>

                    <View style={styles.inputStyles}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/lock.png')}
                          tintColor={
                            this.state.password.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.password}
                          onChangeText={(password) => {
                            this.setState({password: password});
                          }}
                          value={this.state.password}
                          secureTextEntry={this.state.secureTextEntry}
                          lineWidth={1}
                          style={styles.placeholderStyle}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                    </View>
                    <TouchableOpacity style={styles.forgotPasswordSection}>
                      <Text style={styles.forgotPassword}>
                        {STRINGS.login.forgotPassword}
                      </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={this.navigateToHome}
                      style={styles.LoginBtn}>
                      <Text style={styles.loginTxt}>{STRINGS.login.login}</Text>
                    </TouchableOpacity>
                  </View>
                )}

                {!this.state.selectedScreen && (
                  <View style={styles.signUp}>
                    <View style={[styles.inputStyles, {marginBottom: '10%'}]}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/user.png')}
                          tintColor={
                            this.state.registerName.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.userName}
                          lineWidth={1}
                          onChangeText={(registerName) => {
                            this.setState({registerName: registerName});
                          }}
                          style={styles.placeholderStyle}
                          value={this.state.registerName}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                    </View>

                    <View style={[styles.inputStyles, {marginBottom: '10%'}]}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/email.png')}
                          tintColor={
                            this.state.registerEmail.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.email}
                          lineWidth={1}
                          onChangeText={(registerEmail) => {
                            this.setState({registerEmail: registerEmail});
                          }}
                          style={styles.placeholderStyle}
                          value={this.state.registerEmail}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                    </View>

                    <View style={[styles.inputStyles, {marginBottom: '10%'}]}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/phone.png')}
                          tintColor={
                            this.state.registerNumber.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.mobileNumber}
                          lineWidth={1}
                          onChangeText={(registerNumber) => {
                            this.setState({registerNumber: registerNumber});
                          }}
                          style={styles.placeholderStyle}
                          value={this.state.registerNumber}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                    </View>

                    <View style={[styles.inputStyles, {marginBottom: '5%'}]}>
                      <View style={styles.imageDiv}>
                        <Image
                          source={require('../assets/lock.png')}
                          tintColor={
                            this.state.registerPassword.length == 0
                              ? Colors.BASE_GREY
                              : Colors.SKY_BLUE
                          }
                          style={styles.iconStyle}
                        />
                      </View>
                      {/* <View style={styles.passView}> */}
                      <View style={styles.TextFieldDiv}>
                        <TextField
                          label={STRINGS.login.password}
                          onChangeText={(registerPassword) => {
                            this.setState({
                              registerPassword: registerPassword,
                            });
                          }}
                          value={this.state.registerPassword}
                          secureTextEntry={this.state.secureTextEntry}
                          lineWidth={1}
                          style={styles.placeholderStyle}
                          inputContainerStyle={styles.inputContainerStyle}
                          baseColor={Colors.BASE_GREY}
                        />
                      </View>
                      {/* <TouchableOpacity
                          style={styles.eyeView}
                          onPress={this.visiblePassword}>
                          <Image
                            source={require('../assets/eye.png')}
                            style={styles.eyeIconStyle}
                            tintColor={Colors.BASE_GREY}
                          />
                        </TouchableOpacity> */}
                      {/* </View> */}
                    </View>
                    <TouchableOpacity
                      onPress={this.navigateToHome}
                      style={styles.LoginBtn}>
                      <Text style={styles.loginTxt}>
                        {STRINGS.login.register}
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.signupRow}>
                      <View>
                        <Text style={styles.signUpWith}>
                          {STRINGS.login.signUpWith}
                        </Text>
                      </View>
                      <TouchableOpacity>
                        <Image
                          source={require('../assets/facebook.png')}
                          style={styles.iconStyle}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Image
                          source={require('../assets/twitter.png')}
                          style={styles.iconStyle}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity>
                        <Image
                          source={require('../assets/google_plus.png')}
                          style={styles.iconStyle}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </View>
            </GestureRecognizer>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default welcomeScreen;

const styles = EStyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '10rem',
    marginRight: '10rem',
    alignItems: 'center',
  },
  logoTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logoStyle: {
    height: '55rem',
    width: '55rem',
  },
  iconStyle: {
    height: '25rem',
    width: '25rem',
  },
  shop: {
    marginLeft: '10rem',
    color: Colors.SKY_BLUE,
    fontFamily: 'Roboto-Medium',
    fontSize: '28rem',
    letterSpacing: '1rem',
  },
  headerTitle: {
    color: Colors.BLACK,
    fontFamily: 'Roboto-Medium',
    fontSize: '25rem',
    marginBottom: '5rem',
    letterSpacing: '0.9rem',
  },
  headerTitleRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  cart: {
    color: Colors.GREEN,
    fontFamily: 'Roboto-Medium',
    fontSize: '28rem',
    letterSpacing: '1rem',
  },
  skip: {
    color: '#7B7B7B',
    fontFamily: 'Roboto-Regular',
    fontSize: '16rem',
  },
  dashStyle: {
    borderColor: Colors.GREEN,
    borderBottomWidth: 2.5,
    width: '35rem',
  },
  dashStyleGrey: {
    borderColor: Colors.DASH_COLOR,
    borderBottomWidth: 2.5,
    width: '35rem',
  },
  dashStyleGrey1: {
    borderColor: Colors.DASH_COLOR,
    borderBottomWidth: 2.5,
    width: '35rem',
    marginLeft: '5rem',
  },
  dashStyle1: {
    borderColor: Colors.GREEN,
    borderBottomWidth: 2.5,
    width: '35rem',
    marginLeft: '5rem',
  },
  dashView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '10rem',
  },
  inputStyles: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageDiv: {
    width: '10%',
    top: 10,
  },
  TextFieldDiv: {
    width: '80%',
  },
  TextFieldPassDiv: {
    width: '100%',
  },
  inputContainerStyle: {
    width: '100%',
  },
  forgotPasswordSection: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: '30rem',
    width: '95%',
    fontFamily: 'Roboto-Medium',
  },
  forgotPassword: {
    color: Colors.LIGHT_GREY,
    fontFamily: 'Roboto-Regular',
    fontSize: '14rem',
  },
  LoginBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.LIGHT_SKYBLUE,
    borderRadius: '50rem',
    height: '60rem',
    marginTop: '20rem',
    marginLeft: '30rem',
    marginRight: '30rem',
  },
  loginTxt: {
    color: Colors.WHITE,
    fontFamily: 'Roboto-Bold',
    fontSize: '18rem',
  },
  eyeIconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '25rem',
    width: '25rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  eyeView: {
    zIndex: 20,
    left: '-35rem',
    top: 8,
  },
  passView: {
    flexDirection: 'row',
    width: '80%',
    alignItems: 'center',
  },
  placeholderStyle: {
    color: Colors.SKY_BLUE,
    fontFamily: 'Roboto-Regular',
    fontSize: fontSizeValue * 10.7,
  },
  signUpWith: {
    color: Colors.GREY_TEXT,
    fontFamily: 'Roboto-Regular',
    fontSize: fontSizeValue * 12,
  },
  signupRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '25rem',
    marginRight: '25rem',
    marginTop: '30rem',
  },
  iconStyle: {
    height: '30rem',
    width: '30rem',
  },
});
