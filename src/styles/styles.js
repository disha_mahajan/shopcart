import EStyleSheet from 'react-native-extended-stylesheet';
import {fontSize} from '../components/functions/Fontsize';
import {Dimensions} from 'react-native';
import Colors from '../styles/colors';

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: entireScreenWidth / 380});
const fontSizeValue = fontSize();
//common style for form elements

export default styles = EStyleSheet.create({
  //all screen background
  splashScreen: {
    alignContent: 'center',
    flex: 1,
    backgroundColor: Colors.SKY_BLUE,
  },
  screen: {
    alignContent: 'center',
    flex: 1,
    backgroundColor: Colors.WHITE,
  },
});