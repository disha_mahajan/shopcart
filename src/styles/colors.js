
const WHITE = '#FFF';
const BLACK = '#000';
const SKY_BLUE = '#0AA6EA';
const GREY_TEXT = '#8E8B88';
const GREEN = '#18BA18';
const LIGHT_GREY = '#B4B4B4';
const DASH_COLOR = '#DEDEDE';
const BASE_GREY = '#A0A0A0';
const LIGHT_SKYBLUE = '#41CAF1';
const ACTIVE_TAB_COLOR = '#0A94C7'

export default {
    WHITE,
    BLACK,
    SKY_BLUE,
    GREY_TEXT,
    GREEN,
    LIGHT_GREY,
    DASH_COLOR,
    BASE_GREY,
    LIGHT_SKYBLUE,
    ACTIVE_TAB_COLOR
};
