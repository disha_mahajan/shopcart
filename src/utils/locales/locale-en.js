const en = {
  welcome: {
    welcomeTo: 'Welcome to ',
    shopcart: 'Shop cart',
    getStarted: 'GET STARTED',
    welcomeText: 'Each online store has its own store and bag, but if you want to go shopping in multiple places at once, so you only have one bag.'
  },
  login: {
    skip: 'Skip',
    shop: 'Shop',
    cart: 'cart',
    signIn: 'Sign in',
    signUp: 'Sign up',
    email: 'Email',
    password: 'Password',
    forgotPassword: 'Forgot Password?',
    login: 'LOGIN',
    register: 'REGISTER',
    mobileNumber: 'Mobile Number',
    userName: 'User name',
    signUpWith: 'Or sign up with'
  },
  home: {
    signOut: 'Sign out',
    settings: 'Settings',
    accountDetails: 'Account details',
    trackOrder: 'Track order',
    accesories: 'Accesories',
    jewellery: 'Jewellery',
    westernWear: 'Western wear',
    walletsAndBags: 'Wallet & Bags',
    womensWear: "Women's wear",
    mensWear: "Men's wear",
    shoes: 'Shoes',
    clothing: 'Clothing',
    cosmetics: 'Cosmetics',
    capHats: 'Caps & Hats',
    sunglassesAndFrames: 'Sunglasses & Frames'
  }
};

export default en;
