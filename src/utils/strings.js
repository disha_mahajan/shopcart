import LocalizedStrings from 'react-native-localization';
import en from './locales/locale-en';

let strings = new LocalizedStrings({
  en
});

export default strings;