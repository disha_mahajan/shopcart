/**
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Image} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import 'react-native-gesture-handler';
import {createStackNavigator, TransitionPresets} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Splash from './src/views/splashScreen';
import Welcome from './src/views/welcomeScreen';
import signInOrSignup from './src/views/signInOrSignup';
import Home from './src/views/home';
import Drawer from './src/views/drawer';

const HomeStack = createStackNavigator (
  {
    Home: {screen: Home},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
    animationEnabled: true,
    defaultNavigationOptions: {
      ...TransitionPresets.ScaleFromCenterAndroid,
    },
  }
);

const TabBarComponent = props => <BottomTabBar {...props} />;

const MainTabs = createBottomTabNavigator (
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({focused}) => {
          const iconimg = focused
            ? require ('./src/assets/home_white.png')
            : require ('./src/assets/home.png');
          return <Image source={iconimg} style={{height: 25, width: 25}} />;
        },
      },
    },
    HotOffer: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Hot offer',
        tabBarIcon: ({focused}) => {
          const iconimg = focused
            ? require ('./src/assets/offer_white.png')
            : require ('./src/assets/offer.png');
          return <Image source={iconimg} style={{height: 25, width: 25}} />;
        },
      },
    },
    MyCart: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'My cart',
        tabBarIcon: ({focused}) => {
          const iconimg = focused
            ? require ('./src/assets/cart_white.png')
            : require ('./src/assets/cart.png');
          return <Image source={iconimg} style={{height: 25, width: 25}} />;
        },
      },
    },
    Search: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Search',
        tabBarIcon: ({focused}) => {
          const iconimg = focused
            ? require ('./src/assets/search_white.png')
            : require ('./src/assets/search.png');
          return <Image source={iconimg} style={{height: 25, width: 25}} />;
        },
      },
    },
    Profile: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({focused}) => {
          const iconimg = focused
            ? require ('./src/assets/profile_white.png')
            : require ('./src/assets/profile.png');
          return <Image source={iconimg} style={{height: 25, width: 25}} />;
        },
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: 'white',
      activeBackgroundColor: '#41CAF1',
      inactiveTintColor: '#8E8B88',
      inactiveBackgroundColor: '#333333',
      labelStyle: {
        fontSize: 12,
      },
      style: {
        height: 60
      }
    },
  },
);

const DrawerStack = createDrawerNavigator (
  {
    MainTabs: MainTabs,
  },
  {
    initialRouteName: 'MainTabs',
    gesturesEnabled: true,
    mode: 'modal',
    drawerType: 'back',
    overlayColor: 'rgba(0, 0, 0, 0)',
    drawerWidth: '70%',
    edgeWidth: 0,
    animationEnabled: true,
    contentComponent: props => <Drawer {...props} />,
  }
);

const LoginStack = createStackNavigator (
  {
    Welcome: {screen: Welcome},
    Auth: {screen: signInOrSignup},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Welcome',
    animationEnabled: true,
    defaultNavigationOptions: {
      ...TransitionPresets.ScaleFromCenterAndroid,
    },
  }
);

const MainNavigator = createSwitchNavigator (
  {
    Splash: {screen: Splash},
    Login: {screen: LoginStack},
    Home: {screen: DrawerStack},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Splash',
    animationEnabled: true,
    defaultNavigationOptions: {
      ...TransitionPresets.ScaleFromCenterAndroid,
    },
  }
);

const App = createAppContainer (MainNavigator);

export default App;
